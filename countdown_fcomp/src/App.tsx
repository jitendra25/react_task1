import { time } from "console";
import React from "react";
import "./App.css";
interface Props {
  time: number;
}
interface State {
  totalSec: number;
  day: number;
  hour: number;
  min: number;
  sec: number;
}

interface Propss {}
const Countdown: React.FC<Props> = (props) => {
  var id: any;
  const s = props.time;
  let dayy = Math.floor(s / (24 * 3600));
  let houry = Math.floor((s % (24 * 3600)) / 3600);
  let miny = Math.floor(((s % (24 * 3600)) % 3600) / 60);
  let secy = Math.floor(((s % (24 * 3600)) % 3600) % 60);
  const [total, setTotal] = React.useState(props.time);
  const [clea, setClear] = React.useState(false);
  const [day, setDay] = React.useState(dayy === 0 ? -1 : dayy);
  const [hour, setHour] = React.useState(houry === 0 ? -1 : houry);
  const [min, setmin] = React.useState(miny === 0 ? -1 : miny);
  const [sec, setSec] = React.useState(secy === 0 ? -1 : secy);

  React.useEffect(() => {
    if (clea === true) {
      setTotal(0);
      return;
    }

    setTimeout(() => {
      if (total === 0) {
        alert("completed");
        return;
      }
      setTotal((pre) => pre - 1);
    }, 1000);
    /*  if (secc === 0) {
           alert("completed");
           clearInterval(tim);
         }   */
  });
  const clear = () => {
    setClear(true);
  };
  return (
    <div>
      <div className="nav">
        <p>Countdown Timer </p>
        <button onClick={clear}>Clear</button>
      </div>
      <div>
        <div className={"cont"}>
          <h3>Countdown ends in...</h3>
          <div className={"main"}>
            {day !== -1 && (
              <div>
                <span className={"text"}>
                  {Math.floor(total / (24 * 3600))}
                </span>
                <br />
                <span className={"tag"}>Days</span>
              </div>
            )}
            {hour !== -1 && (
              <div>
                <span className={"text"}>
                  {Math.floor((total % (24 * 3600)) / 3600)}
                </span>
                <br />
                <span className={"tag"}>Hours</span>
              </div>
            )}
            {min !== -1 && (
              <div>
                <span className={"text"}>
                  {Math.floor(((total % (24 * 3600)) % 3600) / 60)}
                </span>
                <br />
                <span className={"tag"}>Mins</span>
              </div>
            )}
            {sec !== -1 && (
              <div>
                <span className={"text"}>
                  {Math.floor(((total % (24 * 3600)) % 3600) % 60)}
                </span>
                <br />
                <span className={"tag"}>Secs</span>
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};
const App: React.FC<Propss> = () => {
  return (
    <div className="contain">
      <Countdown time={70} />
    </div>
  );
};
export default App;
