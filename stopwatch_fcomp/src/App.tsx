import React, { useRef } from 'react';
import './App.css';
interface Props{

}
const Watch:React.FC<Props> = () =>{
   var id:any = useRef(null);
  const [time,setTime] = React.useState(0);
  const [start,setStart] = React.useState<boolean>(true);
 /* React.useEffect(()=>{
    return ()=>{
      clearInterval(intervalId);
    }
 },[]) */
  const startt = ()=>{
   
       id.current = setInterval(() => {
           setTime((pre) => pre + 1);
           console.log("heloo")
         }, 1000);
           setStart(false);    
  }
  const pause = ()=>{
    clearInterval(id.current);
    setStart(true);
  }
  const reset =() =>{
    clearInterval(id.current);
     setTime(0);
     setStart(true);
  }
  const conver = (item:number):string=>{
     if(item.toString().length==2)
     return item.toString();
     else{
       var temp:string = "0"+item.toString();
       return temp;
     }
     
  }
  return (
    <div className={"main"}>
      <div className={"time"}>
        <p>{conver(Math.floor(time / 3600))} : </p>
        <p>{conver(Math.floor(time / 60))} : </p>
        <p> {conver(time % 60)}</p>
      </div>
      <div className={"btn"}>
        {start && <button onClick={startt}>start</button>}
        {!start && <button onClick={pause}>Pause</button>}
        <button onClick={reset}>Reset</button>
      </div>
    </div>
  );
}
const App:React.FC<Props> = () =>{
  return (
      <div className={"cont"}>
        <h3>React Stopwatch</h3>
        <Watch />
      </div>
  )
}
export default App;